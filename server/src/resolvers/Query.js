function getAllMessages(parent, args, context, info) {
  return context.prisma.messages();
}

module.exports = {
  getAllMessages,
};
