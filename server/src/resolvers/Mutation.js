async function createMessage(parent, args, context, info) {
  console.log(args);
  const { input } = args;
  return await context.prisma.createMessage({
    text: input.text,
    fromUserId: input.fromUserId,
    likes: 0,
    dislikes: 0,
  });
}

async function setLike(parent, args, context) {
  const { input } = args;
  const message = await context.prisma.$exists.message({
    id: input.id,
  });
  if (!message) {
    throw new Error(`No message found when updating`);
  }
  const likes = message.likes;
  message.likes = likes + 1;
  return message;
}

module.exports = {
  createMessage,
  setLike,
};
