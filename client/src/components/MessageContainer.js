import React, { useEffect, useState } from "react";
import Message from "./Message";
import Logo from "./Logo";
import { GET_ALL_MESSAGES, CREATE_MESSAGE } from "../query/messages";
import { useQuery, useMutation } from "@apollo/client";
function MessageContainer(props) {
  const [inputValue, setInputValue] = useState("");
  const [createMessage, { input: messageData }] = useMutation(CREATE_MESSAGE);
  const { data, loading, error } = useQuery(GET_ALL_MESSAGES);
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    if (!loading) {
      setMessages(data.getAllMessages);
    }
  }, [data]);
  let i = 0;
  const sendMessage = (e) => {
    e.preventDefault();
    createMessage({
      variables: {
        input: {
          text: inputValue,
          fromUserId: +sessionStorage.getItem("userid"),
        },
      },
    }).then(({ data }) => {
      const newMessage = data.createMessage;
      const newMessages = messages.concat(newMessage);
      setMessages(newMessages);
    });
    setInputValue("");
  };
  return (
    <div className="container">
      <Logo />
      <hr />
      <div className="messageList">
        {messages.map((message) => (
          <Message message={message} key={i++} />
        ))}
      </div>
      <div className="messageInput">
        <form onSubmit={(e) => sendMessage(e)}>
          <input
            className="input-message"
            type="text"
            name="textMessage"
            placeholder="enter message text.."
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
          ></input>
          <input type="submit" value="send" className="sendMessageBtn"></input>
        </form>
      </div>
    </div>
  );
}

export default MessageContainer;
