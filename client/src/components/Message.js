import React, { useState } from "react";
import Reply from "./Reply";
import { useMutation } from "@apollo/client";
function Message(props) {
  const [isLiked, setIsLiked] = useState(false);
  const [isDisliked, setIsDisliked] = useState(false);
  const message = props.message;
  const replies = message.replies;
  const [replying, setReplying] = useState(false);
  const userId = message.fromUserId.toString();
  const setLike = (e) => {
    if (isDisliked) {
      setIsDisliked(!isDisliked);
    }
    setIsLiked(!isLiked);
  };
  return (
    <div className="message">
      <div className="userid">
        {userId.length >= 4 ? userId.substr(userId.length - 4, 3) : userId}
      </div>
      <div className="messageText">{message.text}</div>
      <div className="messageReactions">
        <div className="like reaction" onClick={(e) => setLike(e)}>
          <i
            className={isLiked ? "fas fa-thumbs-up liked" : "fas fa-thumbs-up"}
          ></i>
          <span className="like-num">{message.likes}</span>
        </div>
        <div className="dislike reaction">
          <i className="fas fa-thumbs-down"></i>
          <span className="dislike-num">{message.dislikes}</span>
        </div>
      </div>
      <div className="replies">
        {replies && replies.length > 0
          ? replies.map((reply) => <Reply reply={reply} />)
          : null}
      </div>
      <button
        onClick={() => setReplying(!replying)}
        className={replying ? "replyBtn display-none" : "replyBtn"}
      >
        Reply
      </button>
      <form className={replying ? "reply-form" : "reply-form display-none"}>
        <input
          type="text"
          className="input-reply"
          placeholder="Enter your message here.."
        ></input>
        <div className="buttons">
          <input type="submit" value="Send"></input>
          <button className="closeReply" onClick={() => setReplying(!replying)}>
            X
          </button>
        </div>
      </form>
    </div>
  );
}
export default Message;
