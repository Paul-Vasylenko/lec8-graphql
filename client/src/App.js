import { useQuery } from "@apollo/client";
import { useEffect, useState } from "react";
import "./App.css";
import MessageContainer from "./components/MessageContainer";
function App() {
  window.onload = () => {
    const id = Date.now();
    sessionStorage.setItem("userid", JSON.stringify(id));
  };
  return <MessageContainer />;
}

export default App;
