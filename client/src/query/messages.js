import { gql } from "@apollo/client";

export const GET_ALL_MESSAGES = gql`
  query {
    getAllMessages {
      id
      text
      likes
      dislikes
      fromUserId
      replies {
        id
        text
      }
    }
  }
`;

export const CREATE_MESSAGE = gql`
  mutation createMessage($input: MessageCreateInput) {
    createMessage(input: $input) {
      text
      fromUserId
    }
  }
`;
